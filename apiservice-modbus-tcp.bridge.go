/*
 * Copyright (c) 2018-2022 Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */

// Package apiservice_modbus_tcp_bridge implements the modbus tcp
// bridge part of the unified api (https://gitlab.com/perinet/unified-api).
package apiservice_modbus_tcp_bridge

import (
	"encoding/json"
	"io"
	"log"
	"net/http"

	server "gitlab.com/perinet/generic/lib/httpserver"
	rbac "gitlab.com/perinet/generic/lib/httpserver/rbac"
	filestorage "gitlab.com/perinet/generic/lib/utils/filestorage"
	utils "gitlab.com/perinet/generic/lib/utils/webhelper"
)

const (
	API_VERSION      = "20"
	MBUS_CONFIG_FILE = "/var/lib/apiservice_modbus_tcp_bridge/mbus.json"
)

// Data type for ModBusTCPBridgeInfo_Get
type ModBusTCPBridgeInfo struct {
	ApiVersion string `json:"api_version"`
}

// Data type for the SerialInterface of ModBusTCPBridgeConfig
type tty_config struct {
	Device string `json:"device_name"`
	Baud   uint   `json:"baudrate"`
	Mode   string `json:"mode"`
}

// Data type for config object
type ModBusTCPBridgeConfig struct {
	SerialInterface tty_config `json:"serial_interface"`
	Port            uint       `json:"port"`
	AutoDiscover    bool       `json:"auto_discover"`
}

type PathInfo = server.PathInfo

var (
	Logger  log.Logger = *log.Default()
	tty_cfg            = tty_config{
		Device: "",
		Baud:   0,
		Mode:   "",
	}
	config = ModBusTCPBridgeConfig{
		SerialInterface: tty_cfg,
		Port:            502,
		AutoDiscover:    false,
	}
)

func init() {
	Logger.SetPrefix("ApiServiceModbusBridge: ")
	Logger.Println("Starting")

	filestorage.LoadObject(MBUS_CONFIG_FILE, &config)
}

func PathsGet() []PathInfo {
	return []PathInfo{
		{Url: "/modbus-tcp-bridge", Method: server.GET, Role: rbac.NONE, Call: ModBusTCPBridgeInfo_Get},
		{Url: "/modbus-tcp-bridge/config", Method: server.GET, Role: rbac.ADMIN, Call: ModBusTCPBridgeConfig_Get},
		{Url: "/modbus-tcp-bridge/config", Method: server.PATCH, Role: rbac.ADMIN, Call: ModBusTCPBridgeConfig_Set},
		{Url: "/modbus-tcp-bridge/devices", Method: server.GET, Role: rbac.NONE, Call: ModBusTCPBridgeMetric_Get},
		{Url: "/modbus-tcp-bridge/rtu-devices", Method: server.GET, Role: rbac.NONE, Call: ModBusTCPBridgeRtuDevices_Get},
	}
}

// Handler for GET request to url "/modbus-tcp-bridge/"
func ModBusTCPBridgeInfo_Get(w http.ResponseWriter, r *http.Request) {
	var http_status int = http.StatusOK
	var info = ModBusTCPBridgeInfo{
		ApiVersion: API_VERSION,
	}
	res, err := json.Marshal(info)
	if err != nil {
		http_status = http.StatusInternalServerError
		res = json.RawMessage(`{"error": "` + err.Error() + `"}`)
	}
	utils.JsonResponse(w, http_status, res)
}

// Handler for GET request to url "/modbus-tcp-bridge/config"
func ModBusTCPBridgeConfig_Get(w http.ResponseWriter, r *http.Request) {
	var http_status int = http.StatusOK
	res, err := json.Marshal(config)
	if err != nil {
		http_status = http.StatusInternalServerError
		res = json.RawMessage(`{"error": "` + err.Error() + `"}`)
	}
	utils.JsonResponse(w, http_status, res)
}

// Handler for PATCH request to url "/modbus-tcp-bridge/config"
func ModBusTCPBridgeConfig_Set(w http.ResponseWriter, r *http.Request) {
	payload, _ := io.ReadAll(r.Body)

	tmp_config := config

	err := json.Unmarshal(payload, &tmp_config)
	if err != nil {
		utils.EmptyResponse(w, http.StatusBadRequest)
		return
	}
	config = tmp_config
	filestorage.StoreObject(MBUS_CONFIG_FILE, config)
	utils.EmptyResponse(w, http.StatusNoContent)
}

// Handler for GET request to url "/modbus-tcp-bridge/devices"
func ModBusTCPBridgeMetric_Get(w http.ResponseWriter, r *http.Request) {
	// FIXME: metrics will be implemented later...
	utils.EmptyResponse(w, http.StatusNotImplemented)
}

// Handler for GET request to url "/modbus-tcp-bridge/rtu-devices"
func ModBusTCPBridgeRtuDevices_Get(w http.ResponseWriter, r *http.Request) {
	var http_status int = http.StatusOK
	var res []byte
	rtu_devices, err := get_usb_rtus()
	if err != nil {
		utils.EmptyResponse(w, http.StatusBadRequest)
		return
	}
	res, err = json.Marshal(rtu_devices)
	if err != nil {
		utils.EmptyResponse(w, http.StatusBadRequest)
		return
	}
	utils.JsonResponse(w, http_status, res)
}
