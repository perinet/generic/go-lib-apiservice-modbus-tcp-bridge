/*
 * Copyright (c) 2018-2022 Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */

package apiservice_modbus_tcp_bridge

import (
	"encoding/json"
	"errors"
	"os"
	"path/filepath"
	"testing"

	server "gitlab.com/perinet/generic/lib/httpserver"
	rbac "gitlab.com/perinet/generic/lib/httpserver/rbac"
	utils "gitlab.com/perinet/generic/lib/utils/webhelper"
	"gotest.tools/v3/assert"
)

const (
	TMP_CONFIG         = "/tmp/mbusd.conf"
	ORIGIN_CONFIG      = "/etc/mbusd/mbusd.conf"
	TESTDATA_CONFIG    = "testdata/mbusd.conf"
	TMP_SCAN_FOLDER    = "/tmp/"
	ORIGIN_SCAN_FOLDER = "/dev/"
)

func TestPaths(t *testing.T) {
	paths := PathsGet()
	for _, path := range paths {
		switch path.Url {
		case "/modbus-tcp-bridge":
			assert.Assert(t, path.Method == server.GET)
			assert.Assert(t, path.Role == rbac.NONE)
			assert.Assert(t, path.Call != nil)
		case "/modbus-tcp-bridge/config":
			assert.Assert(t, (path.Method == server.GET || path.Method == server.PATCH))
			assert.Assert(t, path.Role == rbac.ADMIN)
			assert.Assert(t, path.Call != nil)
		case "/modbus-tcp-bridge/devices":
			assert.Assert(t, path.Method == server.GET)
			assert.Assert(t, path.Role == rbac.NONE)
			assert.Assert(t, path.Call != nil)
		case "/modbus-tcp-bridge/rtu-devices":
			assert.Assert(t, path.Method == server.GET)
			assert.Assert(t, path.Role == rbac.NONE)
			assert.Assert(t, path.Call != nil)
		default:
			err := errors.New("Not supported API path: " + path.Url)
			assert.NilError(t, err)
		}
	}
}

func TestModBusTCPBridgeInfo(t *testing.T) {
	data := utils.InternalGet(ModBusTCPBridgeInfo_Get)

	var modbus_tcp_bridge_info ModBusTCPBridgeInfo
	err := json.Unmarshal(data, &modbus_tcp_bridge_info)

	assert.NilError(t, err)
	assert.Assert(t, modbus_tcp_bridge_info.ApiVersion == API_VERSION)
}

func TestModBusTCPBridgeConfig(t *testing.T) {
	// Test PATCH / GET
	var serial_device = tty_config{
		Device: "ttyUSB32",
		Baud:   115200,
		Mode:   "8o1",
	}
	var patch_config = ModBusTCPBridgeConfig{
		SerialInterface: serial_device,
		Port:            5522,
		AutoDiscover:    true,
	}

	data, err := json.Marshal(patch_config)
	assert.NilError(t, err)

	utils.InternalPatch(ModBusTCPBridgeConfig_Set, data)

	var get_config ModBusTCPBridgeConfig
	data = utils.InternalGet(ModBusTCPBridgeConfig_Get)
	err = json.Unmarshal(data, &get_config)
	assert.NilError(t, err)

	assert.Assert(t, patch_config == get_config)
}

func TestModBusTCPBridgeRtuDevices(t *testing.T) {
	// Precondition
	rtu_device_dir = TMP_SCAN_FOLDER
	rtu_devices := []string{"ttyUSB8", "ttyACM15"}
	for _, rtu_dev := range rtu_devices {
		path := filepath.Join(TMP_SCAN_FOLDER, rtu_dev)
		dst_file, _ := os.Create(path)
		defer dst_file.Close()
	}

	// Test
	data := utils.InternalGet(ModBusTCPBridgeRtuDevices_Get)

	var result []ModBusTCPBridgeRtuDevice
	err := json.Unmarshal(data, &result)
	assert.NilError(t, err)

	for _, dev := range result {
		assert.Assert(t, dev.DeviceName == rtu_devices[0] || dev.DeviceName == rtu_devices[1])
	}
	// Post condition
	for _, rtu_dev := range rtu_devices {
		path := filepath.Join(TMP_SCAN_FOLDER, rtu_dev)
		os.Remove(path)
	}
}
