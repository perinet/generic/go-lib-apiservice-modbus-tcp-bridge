module gitlab.com/perinet/periMICA-container/apiservice/modbusbridge

go 1.18

require (
	gitlab.com/perinet/generic/lib/httpserver v0.0.0-20221122163058-59cb209fd3f2
	gitlab.com/perinet/generic/lib/utils v0.0.0-20221122162821-91b714770155
	gotest.tools/v3 v3.4.0
)

require (
	github.com/alexandrevicenzi/go-sse v1.6.0 // indirect
	github.com/felixge/httpsnoop v1.0.1 // indirect
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	golang.org/x/exp v0.0.0-20230131160201-f062dba9d201 // indirect
)
