# go-lib-apiservice-modbus-tcp-bridge

The go-lib-apiservice-modbus-tcp-bridge is the implementation of the modbus tcp
bridge part of [unified api](https://gitlab.com/perinet/unified-api).

## References

* [openAPI spec for apiservice-modbus-tcp-bridge] <https://gitlab.com/perinet/unified-api/-/blob/main/services/ModBusTCPBridge_openapi.yaml>
