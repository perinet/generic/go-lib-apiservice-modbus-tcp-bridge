/*
 * Copyright (c) 2018-2022 Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */

package apiservice_modbus_tcp_bridge

import (
	"io/ioutil"
	"strings"
)

// Data type for ModBusTCPBridgeConfig_Get/Set
type ModBusTCPBridgeRtuDevice struct {
	DeviceName string `json:"device_name"`
}

var (
	rtu_device_dir = "/dev/" // Scan folder for rtu devices
)

// Evaluate all ttyACM* and ttyUSB* devices
func get_usb_rtus() ([]ModBusTCPBridgeRtuDevice, error) {
	var result []ModBusTCPBridgeRtuDevice
	files, err := ioutil.ReadDir(rtu_device_dir)
	if err != nil {
		return result, err
	} else {
		for _, file := range files {
			filename := string(file.Name())
			if strings.HasPrefix(filename, "ttyUSB") || strings.HasPrefix(filename, "ttyACM") {
				var device = ModBusTCPBridgeRtuDevice{
					DeviceName: filename,
				}
				result = append(result, device)
			}
		}
	}
	return result, nil
}
